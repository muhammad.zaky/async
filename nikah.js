let biayaNikah = false;

const ngumpulinBiaya = () => new Promise(resolve =>{
    setTimeout(() => {
        biayaNikah = true;
        resolve();
    }, 5000);
})

const nikah = women => {
    if(biayaNikah === false) return Promise.reject("kumpulin biaya dulu woiiii");

    return Promise.resolve(`yeaaa, mau nikah dengan ${women}`);
}

async function main(){
    try {
        await ngumpulinBiaya();
        const menikah = await nikah('dian');
        console.log(menikah)
    } catch (error) {
        console.log(error)
    }
}

main();