let biayaNikah = false;

const kumpulinBiaya = () => new Promise(resolve => {
    setTimeout(() => {
        biayaNikah = true;
        resolve()
    }, 3000)
})

const nikah = women => {
    if(!biayaNikah) return Promise.reject('Belum punya biaya');

    return Promise.resolve(`yeaaa, bulan ini nikah dengan ${women}`)
}

async function main(){
    try {
        await kumpulinBiaya();
        const hasilNikah = await nikah('nurul');
        console.log(hasilNikah);       
    } catch (error) {
        console.log(error)
    }
}

main();